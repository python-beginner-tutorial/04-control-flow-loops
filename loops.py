# loops in python are
# - for
# - while

# for loop

# the for loop is used to iterate through a number finit of elements.
# in python is named and iterable

# Examples are 
#   - range
#   - list

fruit_list = ["apple", "banana", "cherry", "strawbery"]

# you can iterate through a range of numbers using the range() function

# range(upper_bound)  is until and not equal than upper_bound  ( < upper_bound)
# range(lower_bound, upper_bound)  from lower_bound to upper_bound-1 ( < upper_bound)
# range(lower_bound, upper_bound, step) from lower_bound to upper_bound (not included) in number of steps

 for i in range(10):        # 0 - 9
    print(i)

for i in range(1, 5):       # 1 - 4  
    print(i)

for i in range(0, 11, 2):   # 0, 2, 4, 6, 8, 10
    print(i)


for fruit in fruit_list:    # "apple", "banana", "cherry", "strawbery"
    print(fruit)